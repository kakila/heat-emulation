using DifferentialEquations, ModelingToolkit, MethodOfLines
using Plots
using Printf
using CSV, DataFrames

include("DiffReac1D.jl")

@. sigmoid(x, b=1.0, x0=0.0) = 1.0 / (1.0 + exp(-b * (x - x0)))

# Load data 
x0 = "5"
d = DataFrame(CSV.File("../data/144_Temps.csv"; header=1, select=["t", "y", x0]))
Tw = dropmissing(DataFrame(CSV.File("../data/T_water.csv"; header=[1,2,3], select=["time_143_s", "T_water_143_degC"])))
L = 15.0e-2
alpha = 7e-7
Tt = L^2 / alpha 
T = maximum(Tw[!,"time_143_s"]) / Tt
nT = nrow(Tw)
nY = sum(d[!,"t"] .== 0)

# T_water BC
Twater = 15.0
Tsoil = 17.8
@. temp(t; t0=1e-2) = Tsoil + (Twater - Tsoil) * sigmoid(t, 500, t0) +  (sqrt(Tsoil * 0.2*t)) * sigmoid(t, 30, t0)

# Emulate the bottom of the gulley with space dependent diffusivity
a0 = 1.0
@. a_(x) = a0 - a0 * 0.86 * sigmoid(x, 1e2, 1.0)

# Define PDE
pde = DiffReac1D.pde(T_soil=Tsoil, T_water=temp, a=a_, h=0, T=T, L=1.1)
# Semi-Discretize into ODE
prob, dx = DiffReac1D.ode(pde)
# Solve ODE problem
sol = solve(prob, Tsit5())

# Plot results and compare with data
ts = 0:0.01:T
ys = 0:dx:1
y0 = 12e-2 / L
t_ = Tw[!,"time_143_s"] / Tt
y_ = range(0, 1.0, length=nY)
t__ = t_[Int64.(d[!,"t"].+1)]
# Animation
anim = @animate for ti in t_
    plot(ys, sol(ti, ys); xlimits=(0, 1), ylimits=(Twater-1, Tsoil+1), label=@sprintf("sim. time=%.2f", ti), legend=:bottomright)
    scatter!([y0], sol(ti, y0), label=false)

    msk_ = t__ .== ti
    scatter!(y_, d[msk_,x0], alpha=0.3, label=false)
    xlabel!("depth ")
    ylabel!("T deposit [°C]")
end 
gif(anim, "heat1d_dirichlet.gif", fps=25)

# Surface plot
begin
    T_ = getindex.(sol.(ts', ys))
    plt = wireframe(ts, ys, T_; label="sim.", color=reverse(cgrad(:RdYlBu_11)), camera=(70, 30), alpha=0.5)
    #plot3d!(plt, t_[Int64.(d[!,"t"].+1)], y_[Int64.(d[!, "y"].+1)], d[!, x0], label="data", marker=2, linewidth=0, markerstrokewidth=0, alpha=0.1)
    surface!(plt, t_, y_, reshape(d[!, x0], nY, nT), alpha=0.5)
    xlabel!("time")
    ylabel!("depth")
    zlabel!("T deposit [°C]")
end
savefig(plt, "heat1d_dirichlet_surf.png")
display(plt)

# Time series

msk = y_[Int64.(d[!, "y"].+1)] .== y0 
begin
    plt = plot(t_, temp(t_), label="y=0")
    scatter!(plt, t_, Tw[!,"T_water_143_degC"], label="T_water", alpha=0.3)

    plot!(plt, t_, sol(t_, y0), label="y=12")
    scatter!(plt, t_, d[msk, x0], label="data y=12", alpha=0.3)

    xlabel!("time")
    ylabel!("T deposit [°C]")
end
savefig(plt, "heat1d_dirichlet.png")
display(plt)