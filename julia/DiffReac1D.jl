module DiffReac1D

using DifferentialEquations, ModelingToolkit, MethodOfLines

# Parameters, variables, and derivatives
@parameters t x
@variables u(..)
Dt = Differential(t)
Dx = Differential(x)
Dxx = Differential(x)^2

function pde(; T_soil, T_water, a, h, L=1, T=1)

    eq  = Dt(u(t,x)) ~ Dx(a(x) * Dx(u(t,x))) + h * (T_soil - u(t,x))
    bcs = [u(0,x) ~ T_soil,
           u(t,0) ~ T_water(t),
           u(t,L) ~ T_soil]

    # Space and time domains
    domains = [t ∈ IntervalDomain(0.0,T),
               x ∈ IntervalDomain(0.0,L)]

    # PDE system
    @named pdesys = PDESystem(eq,bcs,domains,[t,x],[u(t,x)])
end

function ode(pdesys; dx=nothing, oder=2, L=1.0, N=50)

    if isnothing(dx)
        dx = L / N
    end
    # Method of lines discretization
    order = 2
    discretization = MOLFiniteDifference([x=>dx],t)

    # Convert the PDE problem into an ODE problem
    prob = discretize(pdesys, discretization)
    return prob, dx
end

end