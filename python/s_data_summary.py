"""Build a table with a summary of the input data and results."""

# Copyright (C) 2023 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <juanpablo.carbajal@ost.ch>

import pandas as pd
from mat73 import loadmat
import numpy as np
from data_handling import data_folder

source_folder = data_folder / "Simulated_T_2Dmodel"

deltaT = {"dT1": 3.0, "dT2": 5.5}  # degC

material = ["sand", "mix", "organic"]

hsed_col = "sediment_depth"

units = {"dT": "degC",
         hsed_col: "cm",
         "T_soil": "degC",
         "material": "-",
         "source_filename": "-",
         "T_filename": "-",
         "file_counter": "-"}



def parse_params_from_filename(filename: str):
    parts = filename.split("_")
    d = dict(material=parts[1], dT=deltaT[parts[2]])  # degC
    d[hsed_col] = float(parts[4])  # cm
    return d


def get_temperatures(data: dict):
    return dict(T_soil=np.round(data["Tinf"], 4))


def make_xaxis(length, delta, units):
    x = np.arange(0, length, delta)
    cols = pd.MultiIndex.from_tuples([("x", units)],
                                     names=("coordinate", "units"))
    return pd.DataFrame(data=x, columns=cols)


def make_yaxis(n, delta, units, h_sediment):
    y = np.round(np.linspace(0, (n - 1) * delta, n), int(np.ceil(-np.log10(delta))))
    cols = pd.MultiIndex.from_tuples([("y", h_sediment, units)],
                                     names=("coordinate", hsed_col, "units"))
    return pd.DataFrame(data=y, columns=cols)


def make_taxis(n, delta, units, dT):
    t = np.round(np.linspace(0, (n - 1) * delta, n), int(np.ceil(-np.log10(delta))))
    cols = pd.MultiIndex.from_tuples([("t", dT, units)],
                                     names=("time", "deltaT", "units"))
    return pd.DataFrame(data=t, columns=cols)


def make_Twater(T, units, n):
    cols = pd.MultiIndex.from_tuples([("time", n, "s"),
                                      ("T_water", n, units)],
                                     names=("magnitude", "file_counter", "units"))
    n = T.size
    delta = 60  # s
    t = make_taxis(n, delta, "s", 0).to_numpy()
    return pd.DataFrame(data=np.column_stack((t, T)), columns=cols)


def make_Tsediment(T):
    df = []
    for n in range(T.shape[2]):
        T_ = np.column_stack((np.ones(T.shape[0])*n, T[:, :, n]))
        df_ = pd.DataFrame(data=T_, columns=["t"] + list(range(T.shape[1])))
        df.append(df_)
    df = pd.concat(df, axis=0)
    df.index.name = "y"
    df.columns.name = "x"
    df = df.reset_index(drop=False)
    return df


def main():
    # x-axis is the same for all
    x = make_xaxis(length=35, delta=1, units="cm")
    x_filepath = data_folder / "x_axis.csv"
    x.to_csv(x_filepath, index=False)

    hsed = []
    y_axes = []
    T_water = []
    records = []
    for n, file in enumerate(source_folder.rglob("*.mat")):
        record = parse_params_from_filename(file.name)

        # load data
        data = loadmat(str(file))
        Temps = data["Temps"]

        if Temps.shape[1] != x.shape[0]:
            import pdb; pdb.set_trace()

        # add y axis
        if record[hsed_col] not in hsed:
            y = make_yaxis(n=Temps.shape[0], delta=0.1, units="cm",
                           h_sediment=record[hsed_col])
            hsed.append(record[hsed_col])
            y_axes.append(y)

        # add T_water
        T_ = make_Twater(Temps[0, 0, :], units="degC", n=n)
        T_water.append(T_)

        # get sediment temperature
        T_filepath = data_folder / f"{n:03}_Temps.csv"
        make_Tsediment(Temps).to_csv(T_filepath, index=False)
        record["T_filename"] = str(T_filepath)

        # add BC temperatures
        record.update(get_temperatures(data))

        # add filenames
        record["source_filename"] = str(file.relative_to(*file.parts[:1]))
        # add index
        record["file_counter"] = n
        records.append(record)

    y_axes = pd.concat(y_axes, axis=1).sort_values(by=hsed_col, axis=1)
    y_filepath = data_folder / "y_axis.csv"
    y_axes.to_csv(y_filepath, index=False)

    T_water = pd.concat(T_water, axis=1).sort_values(by="file_counter", axis=1)
    Tw_filepath = data_folder / "T_water.csv"
    T_water.to_csv(Tw_filepath)

    df = pd.DataFrame.from_records(records)
    df.columns = pd.MultiIndex.from_tuples([(c, units[c]) for c in df.columns],
                                           names=["variable", "units"])
    idx = df.swaplevel(0, 1, axis=1).droplevel(0, axis=1).file_counter
    df.set_index(idx, inplace=True)
    df.drop(columns=["file_counter"], level=0, inplace=True)
    df.to_csv(data_folder / "data_index.csv")
    return df, x, y_axes, T_water


if __name__ == "__main__":
    df, x, y, Tw = main()
