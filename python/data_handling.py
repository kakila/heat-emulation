""" Functions to handle data."""
import pdb
# Copyright (C) 2023 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <juanpablo.carbajal@ost.ch>
from pathlib import Path
import pandas as pd
import numpy as np
from collections import namedtuple

from units import Qty

data_folder = Path("../data")

Material = namedtuple("Material", ["thermal_conductivity", "volumetric_heat_capacity"])

materials = dict(
    sand=Material(thermal_conductivity=Qty("2.168 W/m/K"),
                  volumetric_heat_capacity=Qty("3.029e6 J/m^3/K")),
    mix=Material(thermal_conductivity=Qty("1.739 W/m/K"),
                  volumetric_heat_capacity=Qty("2.868e6 J/m^3/K")),
    organic=Material(thermal_conductivity=Qty("0.750 W/m/K"),
                  volumetric_heat_capacity=Qty("3.070e6 J/m^3/K"))
)


def load_index():
    return pd.read_csv(data_folder / "data_index.csv", header=[0, 1],
                       index_col=0)


def mask_by_params(index, *, material: str, deltaT, Tsoil,
                        hsed,
                        deltaT_tol=0.5, Tsoil_tol=1e-3,
                        hsed_tol=0.1):
    index = index.droplevel("units", axis=1)
    msk = index.material == material
    if ~msk.any():
        raise ValueError(f"Material {material} not found.")

    for c, v, t in [("dT", deltaT, deltaT_tol),
                    ("T_soil", Tsoil, Tsoil_tol),
                    ("sediment_depth", hsed, hsed_tol)]:
        msk_ = np.isclose(index[c], v, atol=t)
        if ~msk_.any():
            raise ValueError(f"f{c} {v} not found.")
        msk &= msk_
    return msk


def files_by_counter(index, counter: int):
    return index.T_filename[counter]


def load_data(filepath: str | Path, *, max_time: float = np.inf):
    df = pd.read_csv(filepath, header=0)
    df = df[df.t <= max_time / 60]
    df["t"] = df.t * 60
    return df


def load_axis(id_: str):
    idx = [0,1] if id_ == "x" else [0,1,2]
    df = pd.read_csv(data_folder / f"{id_}_axis.csv", header=idx, index_col=0)
    return df


def load_twater():
    df = pd.read_csv(data_folder / "T_water.csv", header=[0,1,2], index_col=0)
    return df


def diffusivity(id_: str):
    m = materials[id_]
    a = m.thermal_conductivity / m.volumetric_heat_capacity
    return a.to_root_units()


def time_scale(id_: str, *, L: float, Lunits: str) -> Qty:
    a = diffusivity(id_)
    L_ = Qty(L, Lunits)
    return (L_**2 / a).to_root_units()


