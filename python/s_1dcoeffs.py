"""Coefficients of the 1d diffusion-reaction equation.

"""

# Copyright (C) 2023 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <juanpablo.carbajal@ost.ch>

import pandas as pd
import numpy as np
import scipy.interpolate as sci
import scipy.optimize as sco

import seaborn as sns
import matplotlib.pyplot as plt

import data_handling as dh

    
def load(idx, *, hsed: float, dT: float = 3.0, x: float = 2, material: str, **kw):
    """ Load data based on parameters. """
    
    msk = np.isclose(idx.sediment_depth, hsed) & np.isclose(idx.dT, dT)
    msk &= (idx.material == material)
    data_idx = idx[msk].droplevel(1, axis=1)  # drop units

    if data_idx.size > 1:
        import pdb; pdb.set_trace()
        # organic dT2 filename not correct
        data_idx = data_idx[~data_idx.source_filename.str.contains("dT2")]

    y_axis = dh.load_axis("y").xs(f"{hsed}", level="sediment_depth", axis=1).dropna()
    y_axis = y_axis.droplevel("units", axis=1)
    # dimensionless y-direction
    y_axis = y_axis / y_axis.max()

    df = {}
    for f, m in zip(data_idx.T_filename, data_idx.material):

        filepath = ".." / dh.Path(f)
        df_ = dh.load_data(filepath, **kw)[["y", "t", f"{x}"]]
        df_["y"] = y_axis.loc[df_.y.values].values
        df[m] = df_.copy()

    df = pd.concat(df, names=["material"]).droplevel(1)
    return df


def der_yy(y: np.ndarray, u: np.ndarray):
    u_yy = []
    for n in range(u.shape[0]):
        spl = sci.splrep(y, u[n, :].flatten())
        u_yy.append(sci.splev(y, spl, der=2))
    u_yy = np.row_stack(u_yy)
    return u_yy


def der_t(t: np.ndarray, u: np.ndarray):
    u_t = []
    for n in range(u.shape[1]):
        spl = sci.splrep(t, u[:, n])
        u_t.append(sci.splev(t, spl, der=1))
    u_t = np.column_stack(u_t)
    return u_t


def coeffs_1d(t: np.ndarray, y: np.ndarray, u: np.ndarray, u_ref: float,
              *, dim: str = "scalar"):
    """Coefficients in 1D diffusion-reaction equation via interpolation."""
    # Spatial derivatives
    u_yy = der_yy(y, u)
    # Temporal derivatives
    u_t = der_t(t, u)

    q = u_ref - u

    # Data matrices
    if dim == "scalar":
        A = np.column_stack((q.flatten(), u_yy.flatten()))
        x, rnorm = sco.nnls(A, u_t.flatten())
        return x
    else:
        raise NotImplementedError("Other output dimensiuons not implemented")


if __name__ == "__main__":
    idx = dh.load_index()

    hsed = 15.0
    Tsoil = idx.T_soil.to_numpy().flatten()[0]
    msk = dh.mask_by_params(idx, material="sand", deltaT=3.0, Tsoil=Tsoil,
                            Tsoil_tol=0.5, hsed=hsed)
    fname = idx.T_filename[msk].values[0][0]

    x = 5
    df = dh.load_data("../" + fname, max_time=3*3600)[["t", "y", f"{x}"]]
    df.rename(columns={f"{x}": "T_"}, inplace=True)

    t_axis = np.unique(df.t.to_numpy()).flatten()

    y_axis = dh.load_axis("y").xs(f"{hsed:.1f}", level="sediment_depth", axis=1).dropna()
    y_axis = y_axis.droplevel("units", axis=1)
    y_n = np.unique(df.y.to_numpy()).flatten()
    y_axis = y_axis.loc[y_n].to_numpy().flatten() * 100

    u = df.T_.to_numpy().reshape(t_axis.size, -1)
    u_ref = u[:, -1].mean()
    # shrink region of estimation
    t_select = range(60, t_axis.size)
    y_select = range(3, y_axis.size)
    htc, alpha = coeffs_1d(t_axis[t_select], y_axis[y_select],
                           u[t_select, :][:, y_select],
                           u_ref=u_ref)

    u_yy = der_yy(y_axis, u)
    u_t = der_t(t_axis, u)
    q = u_ref - u
    res = u_t - alpha * u_yy - htc * q

    plt.plot(t_axis, res / u)
    #plt.contourf(t_axis, y_axis, res)

    plt.show()



