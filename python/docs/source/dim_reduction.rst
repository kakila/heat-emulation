==========================================
Dimensional reduction of the heat equation
==========================================

Here we explore the idea of reducing a high dimensional heat equation (3D, 2D) into a
one dimensional equation.
The idea is not universal, but illustrates a process that can be generalized.
The reduction, of course, looses information, so we cannot get all the details
and resolution the full model provides.
But, as we see below, there are many situations where the reduced information available
justifies a reduction on teh information provided by the model.

Intuition
=========

:numref:`detailed_domains` shows two examples of detailed domains for heat transport.
Here we are considering homogenous heat transport, so there are no heat sources within the media.
Also, we consider boundary conditions (potentially, time and spatial dependent) in which we know the value of the field (Dirichlet).

.. figure::
   :name: detailed_domains


