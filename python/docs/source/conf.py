# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Emulation Heat 2D'
copyright = '2023, Juan Pablo Carbajal'
author = 'Juan Pablo Carbajal'

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import sys
from pathlib import Path

src_path = Path("../..").resolve()
sys.path.insert(0, str(src_path))

# Detect if latex is generated
latex = "latex" in " ".join(sys.argv)

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosectionlabel",
    "sphinx_autodoc_typehints",
    "sphinx_autodoc_defaultargs",
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinxdoc'
html_static_path = ['_static']

# -- Options for Gallery extension ----------------------------------------------
import re
import os
import sphinx_gallery.sorting
# This allows rendering code in latex mode
class ResetArgv:
    def __repr__(self):
        return 'ResetArgv'

    def __call__(self, sphinx_gallery_conf, script_vars):
        return ['latex']

sphinx_gallery_conf = {
    # path to your example scripts
    "examples_dirs": ["../.."],
    # path to where to save gallery generated output
    "gallery_dirs": ["gen_reports"],
    # directory where function granular galleries are stored
    "backreferences_dir": "gen_modules/backreferences",
    # files executed
    "filename_pattern": f"{re.escape(os.sep)}report_",
    # excluded files
    "ignore_pattern": rf"{re.escape(os.sep)}s_|__init__\.py",
    # order of gallery
    "within_subsection_order": sphinx_gallery.sorting.FileNameSortKey,
    # do not reset on each example
    'reset_modules': (),
    # do not capture matplotlib output 
    # https://sphinx-gallery.github.io/stable/configuration.html#prevent-capture-of-certain-classes
    'ignore_repr_types': r'matplotlib[.](text|axes|legend|lines)',
    # Show memeory usage
    'show_memory': True,
}
if latex:
    sphinx_gallery_conf['reset_argv'] = ResetArgv()

