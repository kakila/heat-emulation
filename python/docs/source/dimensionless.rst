============================
Dimensionless heat equation
============================

It is possible to simulate an infinite number of configurations with a single run
of the PDE solver.
To do that we need to make the equation dimensionless.
Let's write the homogenous heat equation in two dimensional variables :math:`\tilde{t}, \tilde{x}, \tilde{y}`
(I put tilde on those ones, because we are going to get rid of them. Also, doing it with more variable doesn't add any technical complexity):

.. math::
   \frac{\partial u}{\partial \tilde{t}} - \alpha \left(\frac{\partial^2 u}{\partial \tilde{x}^2} + \frac{\partial^2 u}{\partial \tilde{y}^2}\right)= 0
   :label: heat_dim

Now let's re-write the equation in some new dimensionless variables :math:`t, x, y`.
The relation between the two sets of variables is a scaling,

.. math::
   \tilde{t} &= T t\\
   \tilde{x} &= L_x x\\
   \tilde{y} &= L_y y

and the dimensionless derivatives obey

.. math::
   \frac{\partial u}{\partial \tilde{t}} &= \frac{1}{T} \frac{\partial u}{\partial t}\\
   \frac{\partial u}{\partial \tilde{x}} &= \frac{1}{L_x} \frac{\partial u}{\partial x}\\
   \frac{\partial u}{\partial \tilde{y}} &= \frac{1}{L_y} \frac{\partial u}{\partial y}\\

re-writing :eq:`heat_dim` and re-organizing factors, we get

.. math::
   \frac{\partial u}{\partial t} - \frac{\alpha\, T}{L_x^2} \frac{\partial^2 u}{\partial x^2} - \frac{\alpha\, T}{L_y^2} \frac{\partial^2 u}{\partial y^2} = 0
   :label: heat_dimless

The scaling factors :math:`T, L_x` are arbitrary.
However one could use some characteristic lengths for :math:`L_x, L_y`, for example, the lengths of the domain where the heat problem is solved.
The temporal scale we choose it as to reduce the number of paramters in the equation. That is, we can choose

.. math::
   T = \frac{L_x^2}{\alpha}
   :label: time_scale

and equation :eq:`heat_dimless` becomes

.. math::
   \frac{\partial u}{\partial t} - \frac{\partial^2 u}{\partial x^2} - r^2 \frac{\partial^2 u}{\partial y^2} = 0
   :label: heat_dimless_red

where the aspect ratio :math:`r` is

.. math::
   r = \frac{L_x}{L_y}

This results shows that if we solve the equations in the dimensionless variables, we can obtain solutions for an all the domains
that share the same aspect ratio.
That's an infinite set!

Boundary conditions
-------------------

We can consider a large class of boundary conditions (BC) written as a linear operator defined on the boundary of the domain,

.. math::
   \mathcal{L} u(t,x,y) = f(t,x,y) \quad \text{for} \quad (t,x,y) \in \mathcal{T} \cup \Gamma

where :math:`\mathcal{T}` is a time interval and :math:`\Gamma` is the boundary of the domain.
A partition of the boundary :math:`\Gamma = \Gamma_1 \cap \Gamma_2 \ldots` can be used to set
different BC, :math:`\mathcal{L}_i`, on different parts of the boundary.

For example Dirichlet BC can be set with :math:`\mathcal{L} := 1`.
A Robin BC could look like

.. math::
   \mathcal{L} := \left(\frac{\partial}{\partial \tilde{x}} - b\right)

which after changing to dimensionless variables can lead to additional parameters.

Dirichlet 1D
*************
Let's consider the 1D case with the spatial domain being :math:`[0,L]`.
Let's also set a time dependent Dirichlet BC on the left endpoint and a fixed BC on the right endpoint.

.. math::
   \frac{\partial u}{\partial t} - \frac{\partial^2 u}{\partial x^2} &= 0\\
   u(t,0) &= f(T\, t)\\
   u(t,1) &= 0

Here we see how the parameters are not present in the equation but on the BC.
The variable x is defined in :math:`[0,1]` because we divided by :math:`L`.
The time is a number that tells us the duration of the process in units of the diffusive time scale defined in :eq:`time_scale`.
So, for all system with the same diffusive time scale, we just need to solve this equation only once.
Different parameters (diffusivity and/or domain length) would change the speed on which the BC is played.







