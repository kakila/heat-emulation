.. Emulation Heat 2D documentation master file, created by
   sphinx-quickstart on Mon May 22 17:10:15 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Emulation Heat 2D's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   dimensionless
   dim_reduction

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
