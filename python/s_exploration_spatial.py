"""Show that has spatial symmetry.

"""

# Copyright (C) 2023 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <juanpablo.carbajal@ost.ch>

import pandas as pd
import numpy as np
import scipy.interpolate as sci

import seaborn as sns
import matplotlib.pyplot as plt

import data_handling as dh

    
def load(idx, *, hsed: float, dT: float = 3.0, x: float = 2, **kw):
    """ Load data based on parameters. """
    
    msk = np.isclose(idx.sediment_depth, hsed) & np.isclose(idx.dT, dT)
    data_idx = idx[msk].droplevel(1, axis=1)  # drop units
    
    # organic dT2 filename not correct
    data_idx = data_idx[~data_idx.source_filename.str.contains("dT2")]

    y_axis = dh.load_axis("y").xs(f"{hsed}", level="sediment_depth", axis=1).dropna()
    y_axis = y_axis.droplevel("units", axis=1)
    # dimensionless y-direction
    y_axis = y_axis / y_axis.max()
    Ny = 10
    y_new = np.round(np.linspace(0, 1, Ny), 1)

    x_axis = dh.load_axis("x").droplevel("units", axis=1)
    Lx = x_axis.max().values
    # dimensionless x-direction
    x_axis = x_axis / Lx

    Nt = 25

    df = {}
    for f, m in zip(data_idx.T_filename, data_idx.material):

        filepath = ".." / dh.Path(f)
        df_ = dh.load_data(filepath, **kw)[["y", "t", f"{x}"]]
        df_["y"] = y_axis.loc[df_.y.values].values

        Tscale = dh.time_scale(m, L=Lx, Lunits="cm")
        #df_["t"] = df_.t / Tscale.m

        if False:
            t_axis = np.unique(df_.t.to_numpy()).flatten()
            newgrid = np.meshgrid(y_new, np.linspace(0, t_axis.max(), Nt))
            newgrid = np.column_stack([x.flatten() for x in newgrid])

            ax_ = (t_axis.flatten(), y_axis.to_numpy().flatten())
            ax_new = newgrid[:, [1, 0]]

            z_ = df_[f"{x}"].to_numpy().T.reshape(*[x.size for x in ax_])
            df[m] = pd.DataFrame(data={"y": newgrid[:, 0], "t": newgrid[:, 1]})
            df[m][f"{x}"] = sci.RegularGridInterpolator(ax_, z_)(ax_new)
        else:
            df[m] = df_.copy()

    df = pd.concat(df, names=["material"]).droplevel(1)
    return df
    

if __name__ == "__main__":
    idx = dh.load_index()

    # Choose 3 vertical slices and plot the temporal evolution
    x0 = [5]#[5, 15, 30]
    hsed = [6., 12., 24.]
    mats = list(dh.materials.keys())
    if "df" not in locals():
        df = {}
        for x in x0:
            for h in hsed:
                xh_ = (x, h)
                df[xh_] = load(idx, max_time=3*3600, x=x, hsed=h).rename(columns={f"{x}": "T_"})
        df = pd.concat(df, names=["x", "hsed"] + df[xh_].index.names).reset_index()
    
    # Compare same layer below water
    msk = None
    y0 = [0.1, 0.5, 0.9]
    for y in y0:
        if msk is None:
            msk = np.isclose(df.y, y)
        else:
            msk |= np.isclose(df.y, y)
    df_ = df[msk]

    sns.relplot(data=df_, x="t", y="T_",
                style="y", hue="hsed", col="material", kind="line", estimator=None,
                palette="vlag")

    plt.show()
