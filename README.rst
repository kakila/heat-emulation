==============================
Emulation of 2D heat equation
==============================

Exploration of emulation and modelling of a 2D heat transfer problem.

Python docs at https://kakila.gitlab.io/heat-emulation/python/docs/
